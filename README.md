# Blog App

## Happy coding!

##### How To Use?

- git clone
- yarn install or npm install
- yarn start or npm start

##### Documentation?

- http://localhost:4000/api-doc

##### Capture?

![my blog](/Capture_blog.PNG)

##### Aptitude test

![my aptitude test](/Capture_test_zettabyte.PNG)
