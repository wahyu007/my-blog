// require("dotenv/config");
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const PORT = 4000;

// start mongo init
const uri =
  "mongodb+srv://wahyu:EYhpKoRcZWh0Nqq8@cluster0.tw8dl.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
const options = { useNewUrlParser: true, useUnifiedTopology: true };
mongoose.set("useFindAndModify", true);

// cors and body-parser
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// swagger documentation
const swaggerUi = require("swagger-ui-express");
swaggerDocument = require("./app/documentation/swagger.json");

app.get("/", (req, res) => {
  res.status(200).send({
    status: "OK",
    documentation: "http://localhost:4000/api-doc",
  });
});

app.use("/api-doc", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// router
require("./app/router/router")(app);

// run server
mongoose
  .connect(uri, options)
  .then(() => {
    app.listen(PORT, () => {
      console.info(`App listening at port http://localhost:${PORT}`);
    });
  })
  .catch((err) => {
    throw err;
  });
