module.exports = (app) => {
  const content = require("../controller/content");
  const comment = require("../controller/comment");
  let router = require("express").Router();

  // comment routers

  router.get("/comments", comment.findAll);
  router.post("/comment/create/:id", comment.create);
  router.get("/comment/:id", comment.getComment);
  router.put("/comment/update/:id", comment.update);
  router.delete("/comment/delete/:id", comment.delete);

  // content routers

  router.get("/contents", content.findAll);
  router.get("/content/search/:content", content.searchContent);
  router.get("/content/sort/date/:sort", content.sortContent);
  router.get("/content/:content", content.getContent);
  router.get("/content/page/:page", content.page);
  router.post("/content/create", content.create);
  router.put("/content/update/:id", content.update);
  router.delete("/content/delete/:id", content.delete);

  // base path
  app.use("/api/v1/blog", router);
};
