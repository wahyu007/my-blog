const db = require("../model");
const Content = db.Content;
// Content.populate("comments");

exports.findAll = (req, res) => {
  Content.find()
    .populate("comments")
    .then((data) => {
      res.status(200).send({
        count: data.length,
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error on Content",
        response: err.message || "Some error while retrieving content",
      });
    });
};

exports.searchContent = (req, res) => {
  content = req.params.content;

  if (!content) {
    res.status(401).send({
      message: "Error on Content",
      response: "Please input param content",
    });

    return;
  }
  Content.find({
    content: { $regex: ".*" + content + ".*", $options: "i" },
  })
    .then((result) => {
      res.status(200).send({
        count: result.length,
        response: result,
      });
    })
    .catch((err) => {
      res.status(400).send({
        message: "Error on Content",
        response: err.message,
      });
    });
};

exports.sortContent = (req, res) => {
  let sort = req.params.sort;

  if ((sort != "asc") & (sort != "desc")) {
    res.status(401).send({
      message: "Error on Content Please param asc or desc",
    });

    return;
  }
  Content.find()
    .sort({ createdAt: sort })
    .then((result) => {
      res.status(200).send({
        count: result.length,
        sort: sort,
        response: result,
      });
    })
    .catch((err) => {
      res.status(400).send({
        message: "Error on Content",
        response: err.message,
      });
    });
};

exports.getContent = (req, res) => {
  Content.findById(req.params.id)
    .populate("comments")
    .then((result) => {
      res
        .status(200)
        .send({
          message: "None",
          response: result,
        })
        .catch((err) => {
          res.status(400).send({
            message: "Error on Content",
            response: err.message,
          });
        });
    });
};

exports.page = (req, res) => {
  let perPage = 5;
  let page = req.params.page > 0 ? req.params.page : 0;

  Content.find()
    .limit(perPage)
    .skip(perPage * page)
    .sort({ createdAt: "asc" })
    .exec((err, events) => {
      Content.countDocuments().exec((err, count) => {
        res.status(200).send({
          page: page,
          pages: count / perPage,
          response: events,
        });
      });
    });
};

exports.create = (req, res) => {
  const body = req.body;
  if (!body.creator || !body.content) {
    res.status(401).send({
      message: "Error on Content",
      response: `ValidationError: Todo validation failed: title: ${body.creator}, status: ${body.content}`,
    });

    return;
  }

  Content.create(req.body)
    .then((data) => {
      res.status(201).send({
        message: "None",
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message: "none",
        response:
          err.message || "Some error ocured while creating the content.",
      });
    });
};

exports.update = (req, res) => {
  const body = req.body;
  if (!body.content || !body.creator) {
    res.status(401).send({
      message: "Error on Content",
      response: `ValidationError: Todo validation failed: title: ${body.content}, status: ${body.creator}`,
    });

    return;
  }

  Content.findByIdAndUpdate(req.params.id, body)
    .then((data) => {
      res
        .status(200)
        .send({ message: "Content Successfully Edited", response: body });
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error on Content",
        response: err.message,
      });
    });
};

exports.delete = async (req, res) => {
  const id = req.params.id;

  if (!id) {
    res.status(401).json({
      status: 401,
      errorMessage: `ValidationError: _id or required body property not defined.`,
    });

    return;
  }

  await Content.findByIdAndRemove(id)
    .then((data) => {
      res.send({
        message: "Comment successfully removed",
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message: "Remove comment not implement",
        response: err.message,
      });
    });
};
