const db = require("../model");
const Comment = db.Comment;
const Content = db.Content;

exports.findAll = async (req, res) => {
  await Comment.find()
    .then((data) => {
      res.status(200).send({
        count: data.length,
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        response: "Error on comment!",
        response: err.message || "Error while retrieving comment",
      });
    });
};

exports.getComment = async (req, res) => {
  await Comment.findById(req.params.id)
    .then((result) => {
      res.status(200).send({
        message: "None",
        response: result,
      });
    })
    .catch((err) => {
      res.status(400).send({
        message: "Error on Content",
        response: err.message,
      });
    });
};

exports.create = async (req, res) => {
  const body = req.body;
  if (!body.name || !body.comment) {
    res.status(401).send({
      message: "Error on Comment",
      response: `ValidationError: Todo validation failed: title: ${body.name}, status: ${body.comment}`,
    });

    return;
  }

  await Comment.create(body)
    .then((data) => {
      return Content.findOneAndUpdate(
        { _id: req.params.id },
        { $push: { comments: data._id } },
        { new: true }
      );
    })
    .then((comment) => {
      res.send(201).send(comment);
    })
    .catch((err) => {
      res.status(500).send({
        message: "none",
        response:
          err.message || "Some error ocured while creating the content.",
      });
    });
};

exports.update = async (req, res) => {
  const body = req.body;
  if (!body.name || !body.comment) {
    res.status(401).send({
      message: "Error on Comment",
      response: `ValidationError: Todo validation failed: title: ${body.name}, status: ${body.comment}`,
    });

    return;
  }

  await Comment.findByIdAndUpdate(req.params.id, body)
    .then((data) => {
      res
        .status(200)
        .send({ message: "Comment Successfully Edited", response: body });
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error on Comment",
        response: err.message,
      });
    });
};

exports.delete = async (req, res) => {
  const id = req.params.id;

  if (!id) {
    res.status(401).json({
      status: 401,
      errorMessage: `ValidationError: _id or required body property not defined.`,
    });

    return;
  }

  await Comment.findByIdAndRemove(id)
    .then((data) => {
      res.send({
        message: "Comment successfully removed",
        response: data,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message: "Remove comment not implement",
        response: err.message,
      });
    });
};
