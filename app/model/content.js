var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var ContentSchema = new Schema(
  {
    creator: {
      type: String,
      required: true,
    },
    content: {
      type: String,
      required: true,
    },
    comments: [
      {
        type: Schema.Types.ObjectId,
        ref: "Comment",
      },
    ],
  },
  { timestamps: true }
);

let Content = mongoose.model("Content", ContentSchema);

module.exports = Content;
